<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class step extends Controller{
    
    public function step1(){
    	return view ('users.step1');
    }
    public function step2(request $request){
    	$a=$request->input('a');
    	return view ('users.step2',compact('a'));
    }
    public function step3(request $request){
        $a=$request->input('a');
        $b=$request->input('b');
        return view ('users.step3',compact('a','b'));
    }
    public function racun (request $request){
        $a=$request->input('a');
        $b=$request->input('b');
        $op=$request->input('op');

        if($op=='+'){
                $rez=$a+$b; 
            }elseif($op=='-'){
                $rez=$a-$b; 
            }elseif($op=='*'){
                $rez=$a*$b;
            }else{
                if($b==0) {
                    $rez="Djeljenje s 0 nije dozvoljeno!";  
                }else{
                    $rez=$a/$b; 
                } 
            }
        return view ('users.rezultat1',compact('rez'));
    }
     
}
