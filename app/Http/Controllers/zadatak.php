<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class zadatak extends Controller
{	
    public function index ($broj){
    	 $rezultat=0;
         $all="";
         $matches=0;

    	 if ($broj<=10) {
    	 	$rezultat=1;
    	 	for ($i=1; $i<=$broj; $i++) { 
    	 		$rezultat=$rezultat*$i;
    	 	}
    	 } elseif ($broj>=11 && $broj<=20) {
    	 	$rezultat=0;
    	 	for ($i=0; $i<=$broj; $i++) { 
    	 		$rezultat=$rezultat+$i;
    	 	}
    	 }  else {
            $matches=2;
            $all="1, 2";
            for ($prost=3; $prost<=$broj; $prost+=2) { 
                $flag=1;
                for($i=2;$i<=$prost/2;$i++){
                    if ($prost%$i==0) $flag=0;
                }
                if ($flag==1) {
                    $matches++;
                    $all=$all . ", " . $prost;
                }
            }
        }

         return view ('users.zadatak', compact('broj','rezultat','all','matches'));
    }
}
