<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class menus extends Controller
{
    public function odabir(){

    	return view ('users.menus');
    }

    public function obrada(request $request){
    	//Unos
    	$vijesti_main=$request->input('vijesti_main');
    		$vijesti=$request->input('vijesti');
    	$sport_main=$request->input('sport_main');
    		$sport=$request->input('sport');
		$showbiz_main=$request->input('showbiz_main');
			$showbiz=$request->input('showbiz');

		//Obrada
		$vijesti_arr=array_diff(explode(",", $vijesti),array(""));
		$sport_arr=array_diff(explode(",", $sport),array(""));
		$showbiz_arr=array_diff(explode(",", $showbiz),array(""));


    	return view ('users.obrada',compact('vijesti_arr','sport_arr','showbiz_arr','vijesti_main','sport_main','showbiz_main'));
    }
}
