<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class digitron extends Controller
{
	    public function index(){
	    	return view('users.digitron');
	    }


	    public function racun(Request $request){
	     	$rez=0;
	    	$a=$request->input('a');
	    	$b=$request->input('b');
	    	$op=$request->input('op');
	    	
	    	if($op=='+'){
	    		$rez=$a+$b;	
	    	}elseif($op=='-'){
	    		$rez=$a-$b;	
	    	}elseif($op=='*'){
	    		$rez=$a*$b;
	    	}else{
	    		if($b==0) {
	    			$rez="Djeljenje s 0 nije dozvoljeno!";	
	    		}else{
	    			$rez=$a/$b;	
	    		} 
	    	}

			return view('users.rezultat',compact('rez'));
	    	
	    }

}
