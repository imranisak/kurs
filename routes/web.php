<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/kvadrat/{broj}', 'kvadrat@index');
Route::get('/zadatak/{broj}', 'zadatak@index');
Route::get('/djeljivost/{broj}', 'djeljivost@index');
Route::get('/djeljivost2/{broj}', 'djeljivost2@index');
Route::get('/digitron', 'digitron@index');
Route::get('/step1', 'step@step1');
Route::post('/step2', 'step@step2');
Route::post('/step3', 'step@step3');
Route::post('/rezultat', 'digitron@racun');
Route::post('/rezultat1', 'step@racun');
Route::get('/menus', 'menus@odabir');
Route::post('/menus2', 'menus@obrada');
Route::get('/bootstrap','bootstrap@main');
Route::get('/blogs','BlogController@index');
Route::get('/blog/{id}','BlogController@show');
Route::get('/unos','BlogController@create');
Route::post('/snimanje','BlogController@store');
Route::get('/brisanje/{id}','BlogController@destroy');
Route::get('/loop','loop@loop1');
//Route::resources('/blogs','BlogController');

//Route::get('/hello', 'UsersController@index');
//Route::get('/test2', 'Controller2@index2');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
