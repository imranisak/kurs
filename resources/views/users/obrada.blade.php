<!DOCTYPE html>
<html>
<head>
	<title>Rezultat</title>
</head>
<body>

@if (count ($vijesti_arr)>0)
<ul> <h3>{{$vijesti_main}}</h3>
	@foreach($vijesti_arr as $v)
		<!--@if (count ($v)>0)-->
	 		<li>{{$v}}</li>
		<!--@endif-->
	@endforeach
</ul>
@endif

@if (count ($sport_arr)>0)
<ul> <h3>{{$sport_main}}</h3>
	@foreach($sport_arr as $s) <li>{{$s}}</li>
	@endforeach
</ul>
@endif

@if (count ($showbiz_main)>0)
	@if (count ($showbiz_arr)>0)
		<ul> <h3>{{$showbiz_main}}</h3>
			@foreach($showbiz_arr as $sh) <li>{{$sh}}</li>
			@endforeach
		</ul>
	@endif
@endif

<a href="/"><h3>Home</h3></a>
</body>
</html>