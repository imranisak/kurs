<!DOCTYPE html>
<html>
<head>
@yield('head')
<script type="css/text" src="css/moja.css"></script>
</head>

<body>
@yield('header')<br>
@yield('menu')<br>
@yield('content')<br>
@yield('footer')<br>
</body>
</html>